import os
import lib/opthandler, lib/colours
import strutils, strformat, re
var
    p = newOptHandler()
    recursive = p.hasFlag("r")
    recursionDepth = p.intFlag("r", 1)
    matchCase = p.hasFlag("c")
    verbose = p.hasFlag("v")
    styling = not p.hasFlag("s")
    showLine = p.hasFlag("l")
    search = p.commands.join(" ").strip()
    cwd = getCurrentDir()
    matches = newSeq[string]()
    matchesLn = newSeq[string]()

proc wd(dir: string, depth:int = 0) =
    if verbose: echo "Entering " & dir
     
    for kind, path in walkDir(dir):
        if verbose: echo fmt"Checking {kind} " & path
        if kind == pcDir:
            if depth < recursionDepth and recursive:
                wd path, depth+1
            
        elif kind == pcFile:
            var ln = 0
            for line in readFile(path).split("\n"):
                if (matchCase and line.contains(search)) or line.toLowerAscii().contains(search.toLowerAscii()):
                    if not styling:
                        matches.add path & " in Line#" & ln.intToStr
                        if showLine:
                            matchesLn.add "[ " & line.strip() & " ]"
                    else:
                        matches.add path.yellow & " in Line#" & ln.intToStr.green
                        if showLine:
                            let r = re(search, {reIgnoreCase, reStudy})
                            matchesLn.add "[ " & line.strip().replace(r, search.blue) & " ]"
                ln += 1
   
proc main() =
    if p.hasFlag("h") or search == "":
        echo "==== DCHK ===="
        echo ""
        echo "dchk [search] [-c] [-r]/[-r:2] [-s] [-l] "
        echo ""
        echo "-s : if found, disables styling/colours"
        echo "-l : if found, adds line that matches was found on to output"
        echo "-c : if found, search requires cases to match"
        echo "-v : if found, print more messages"
        echo "-r : if found, recursive search through subdirectories"
        echo "   ^ takes optional arguement of recursion depth, in the format of -r:<depth value>"
        return
        
    if verbose:
        echo "Recursive:         " & $recursive
        echo "Recursion Depth:   " & $recursionDepth
        echo "Require Case:      " & $matchCase
        echo "Styling:           " & $styling
        echo "Show lines:        " & $showLine
        echo "Searching for:     " & search
        
    wd cwd
    echo matchesLn
    if matches.len > 0:
        var i = 0
        echo fmt"{matches.len} matches found;"
        for ln in matches:
            echo i.intToStr & ": " & ln
            if matchesLn.len > 0:
                echo matchesLn[i]
            i += 1
    else:
        echo "No matches."
        
main()
                
