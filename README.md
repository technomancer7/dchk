# dchk
CLI app for file content searching.

Either compile from source with Nim compiler, `nim c dchk.nim`, or drop the pre-compiled linux amd64 binary in a PATH directory.

```sh
dchk [search] [-c] [-r]/[-r:2] [-s] [-l] 

-s : if found, disables styling/colours
-l : if found, adds line that matches was found on to output
-c : if found, search requires cases to match
-v : if found, print more messages
-r : if found, recursive search through subdirectories
   ^ takes optional arguement of recursion depth, in the format of -r:<depth value>

```
